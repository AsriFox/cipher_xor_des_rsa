﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Cipher_Sym
{
    class Cipher_RSA
    {
        public static byte[] Crypt(byte[] Inch, UInt32 Basen, UInt32 Key)
        {
            BigInteger Ins = 0;
            for (int q = 0; q < Inch.Length; q++)
                { Ins += (BigInteger)((long)1 << (8 * q)) * Inch[Inch.Length - 1 - q]; }

            // Возведение в степень по модулю:
            BigInteger Res = BigInteger.ModPow(Ins, Key, (BigInteger)Basen);

            var Result = new byte[Inch.Length];
            for (int q = Inch.Length - 1; q >= 0; q--)
                { Result[q] = (byte)(Res % 256); Res /= 256; }
            return Result;
        }

        public static UInt32 KeyGen(out UInt32 OpenKey, out UInt32 SecretKey, bool NoOK)
        {
            var Rand = new Random();
            // Получение двух простых чисел:
            UInt32 p, q;
            while (true)
            {
                // Генерация двух случайных чисел:
                p = (UInt32)(Rand.Next(0, 65535));
                q = (UInt32)(Rand.Next(0, 65535));

                // Установка "близости" чисел:
                if (p > q) { while (p - q > 16384) { q += 16384; } }
                if (q > p) { while (q - p > 16384) { p += 16384; } }

                // Проверка простоты чисел:
                if ((p % 2 == 1) && (q % 2 == 1))
                    { if (Simple_Ferma(Rand, p) && Simple_Ferma(Rand, q)) { break; } }
            }

            // Получение ключевых чисел:
            UInt32 n = p * q;
            UInt32 f = (p - 1) * (q - 1);
            while (true)
            {
                // Выбор открытого ключа:
                if (NoOK) { OpenKey = 65537; }
                else
                {
                    if (f > Int32.MaxValue)
                        { OpenKey = (UInt32)(Rand.Next()); }
                    else { OpenKey = (UInt32)(Rand.Next(0, (int)(f - 1))); }
                }
                // Поиск закрытого ключа:
                long x, y, r;
                extended_euclid(f, OpenKey, out x, out y, out r);
                if (r == 1)
                {
                    if (y < 0) { SecretKey = (UInt32)((long)(f) + y); }
                    else { SecretKey = (UInt32)(y); }

                    break;
                }
            }
            return n;
        }

        private static bool Simple_Ferma(Random Rand, UInt32 Num)
        {
            // Тест простоты Ферма:
            ushort k = 0;
            while (k <= 100)
            {
                BigInteger a = Rand.Next(0, (int)(Num));
                a = (a % (BigInteger)(Num - 2)) + 2;
                if (gcd((long)(a), Num) != 1) { return false; }

                BigInteger c = BigInteger.ModPow(a, Num - 1, (BigInteger)Num);
                if (c != 1) { return false; }
                k++;
            }
            return true;
        }

        private static void extended_euclid(long a, long b, out long x, out long y, out long d)
        {
            long aa = a, bb = b;
            long q, r, x1, x2, y1, y2;

            if (b == 0)
            {
                d = a; x = 1; y = 0;
                return;
            }

            x2 = 1; x1 = 0; y2 = 0; y1 = 1;

            while (bb > 0)
            {
                q = aa / bb;
                r = aa - q * bb;
                x = x2 - q * x1;
                y = y2 - q * y1;
                aa = bb; bb = r;
                x2 = x1; x1 = x;
                y2 = y1; y1 = y;
            }
            d = aa; x = x2; y = y2;
        }
        
        private static long gcd(long a, long b)
        {
            long temp, aa = a, bb = b;
            while (true)
            {
                temp = aa % bb;
                if (temp == 0) { return bb; }
                aa = bb;
                bb = temp;
            }
        }

        /*
        private static ulong mul(ulong a, ulong b, ulong m)
        {
            if (b == 1) { return a; }
            if (b % 2 == 0)
            {
                ulong t = mul(a, b / 2, m);
                return (2 * t) % m;
            }
	        return (mul(a, b-1, m) + a) % m;
        }

        private static ulong pows(ulong a, ulong b, ulong m){
            if (b == 0) { return 1; }
            if (b % 2 == 0)
            {
                ulong t = pows(a, b / 2, m);
                return mul(t, t, m) % m;
            }
	        return (mul(pows(a, b-1, m) , a, m)) % m;
        }
        */
    }
}
