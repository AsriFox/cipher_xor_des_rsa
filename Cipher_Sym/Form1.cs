﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cipher_Sym
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Sel_Execute_Fwd.Checked = true;
            Sel_Execute_Rev.Checked = false;
            LB_CipherMethod.SelectedIndex = 0;

            Check_OK.Checked = true;
            TB_RSA_0.Enabled = false; TB_RSA_0.Visible = false;
            TB_RSA_1.Enabled = false; TB_RSA_1.Visible = false;
            TB_RSA_2.Enabled = false; TB_RSA_2.Visible = false;
        }

        private void Bn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Sel_Execute_Fwd_CheckedChanged(object sender, EventArgs e)
        {
            TB_Plain.ReadOnly   = false;
            TB_Cipher.ReadOnly  = true;
        }

        private void Sel_Execute_Rev_CheckedChanged(object sender, EventArgs e)
        {
            TB_Plain.ReadOnly   = true;
            TB_Cipher.ReadOnly  = false;
        }

        private void Check_OK_CheckedChanged(object sender, EventArgs e)
        {
            TB_RSA_1.ReadOnly = !(Check_OK.Checked);
            if (!Check_OK.Checked) { TB_RSA_1.Text = "65537"; }
        }

        private void LB_CipherMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (LB_CipherMethod.SelectedIndex)
            {
                case 0: // Метод исключающего ИЛИ:
                    Label_NK.Visible = false; Label_SK.Visible = false;
                    Check_OK.Enabled = false; Check_OK.Visible = false;
                    TB_RSA_0.Enabled = false; TB_RSA_0.Visible = false;
                    TB_RSA_1.Enabled = false; TB_RSA_1.Visible = false;
                    TB_RSA_2.Enabled = false; TB_RSA_2.Visible = false;
                    Num_KeyLength.Enabled = true; Num_KeyLength.Visible = true;
                    TB_Key.Enabled = true; TB_Key.Visible = true;
                    TB_Plain.Height = 400;

                    Num_KeyLength.Value = 16;
                    Num_KeyLength.ReadOnly = false;
                    Num_KeyLength.Enabled = true;
                    break;

                case 1: // Метод DES-ECB:
                    Label_NK.Visible = false; Label_SK.Visible = false;
                    Check_OK.Enabled = false; Check_OK.Visible = false;
                    TB_RSA_0.Enabled = false; TB_RSA_0.Visible = false;
                    TB_RSA_1.Enabled = false; TB_RSA_1.Visible = false;
                    TB_RSA_2.Enabled = false; TB_RSA_2.Visible = false;
                    Num_KeyLength.Enabled = true; Num_KeyLength.Visible = true;
                    TB_Key.Enabled = true; TB_Key.Visible = true;
                    TB_Plain.Height = 400;

                    Num_KeyLength.Value = 8;
                    Num_KeyLength.ReadOnly = true;
                    Num_KeyLength.Enabled = false;
                    break;

                case 2: // Метод RSA:
                    Label_NK.Visible = true; Label_SK.Visible = true;
                    Check_OK.Enabled = true; Check_OK.Visible = true;
                    TB_RSA_0.Enabled = true; TB_RSA_0.Visible = true;
                    TB_RSA_1.Enabled = true; TB_RSA_1.Visible = true;
                    TB_RSA_2.Enabled = true; TB_RSA_2.Visible = true;
                    Num_KeyLength.Enabled = false; Num_KeyLength.Visible = false;
                    TB_Key.Enabled = false; TB_Key.Visible = false;
                    TB_Plain.Height = 372;
                    break;
            }
        }

        // Генератор случайного ключа:
        private void Bn_Keygen_Click(object sender, EventArgs e)
        {
            switch (LB_CipherMethod.SelectedIndex)
            {
            case 0: // Для простого XOR-шифра:
                UInt16 size = (ushort)(Num_KeyLength.Value);
                var rand = new Random();
                string key = "";
                while (key.Length < size)
                    { key += (char)(rand.Next(0, 256)); }
                TB_Key.Text = key;
                break;

            case 1: // Для шифрования DES-ECB:
                TB_Key.Text = Cipher_DES.KeyGen();
                break;

            case 2: // Для шифрования RSA:
                UInt32 NK, OK, SK;
                this.Cursor = Cursors.WaitCursor;
                NK = Cipher_RSA.KeyGen(out OK, out SK, !(Check_OK.Checked));
                this.Cursor = DefaultCursor;
                TB_RSA_0.Text = NK.ToString();
                TB_RSA_1.Text = OK.ToString();
                TB_RSA_2.Text = SK.ToString();
                break;
            }
        }

        // Произвести шифрование или дешифрование:
        private void Bn_Execute_Click(object sender, EventArgs e)
        {
            if (LB_CipherMethod.SelectedIndex == 2)
            {   // Шифрование RSA:
                UInt32 basen, key = 0;
                if (UInt32.TryParse(TB_RSA_0.Text, out basen))
                {
                    if (Sel_Execute_Fwd.Checked) { key = UInt32.Parse(TB_RSA_1.Text); }
                    if (Sel_Execute_Rev.Checked) { key = UInt32.Parse(TB_RSA_2.Text); }
                    UInt32 Key2 = UInt32.Parse(TB_RSA_2.Text);

                    string workstr = "";
                    if (Sel_Execute_Fwd.Checked) { workstr = TB_Plain.Text;  }
                    if (Sel_Execute_Rev.Checked) { workstr = TB_Cipher.Text; }
                    if (workstr.Length > 0)
                    {
                        Progress_Execute.Maximum = workstr.Length;
                        Progress_Execute.Value = 0;

                        string result = "";
                        this.Cursor = Cursors.WaitCursor;
                        while (workstr.Length > 0)
                        {
                            // Разбиение текста на блоки допустимой величины:
                            byte q = 0;
                            UInt32 Code = 0;
                            if (Sel_Execute_Fwd.Checked)
                            {
                                while ((q < 4) && (q < workstr.Length))
                                {
                                    uint NewCode = (uint)(1 << (8 * q)) * (uint)(workstr[q] % 256);
                                    if (Code + NewCode > basen) { break; }
                                    Code += NewCode;
                                    q++;
                                }
                            }
                            if (Sel_Execute_Rev.Checked)
                            {
                                q = 4;
                                for (int k = 0; k < 4; k++)
                                {
                                    Code += (uint)(1 << (8 * k)) * (uint)((workstr[k] - 32) % 256);
                                }
                            }

                            // Возведение в степень по модулю:
                            BigInteger Res = BigInteger.ModPow((BigInteger)Code, key, (BigInteger)basen);

                            // Кодирование (unsigned char):
                            if (Sel_Execute_Fwd.Checked)
                            {
                                for (int k = 0; k < 4; k++)
                                    { result += (char)((Res % 256) + 32); Res /= 256; }
                            }
                            if (Sel_Execute_Rev.Checked)
                            {
                                while (Res > 0)
                                    { result += (char)(Res % 256); Res /= 256; }
                            }
                            int m = result.Length;
                            workstr = workstr.Remove(0, q);
                            Progress_Execute.Value = Progress_Execute.Maximum - workstr.Length;
                        }
                        this.Cursor = DefaultCursor;

                        if (Sel_Execute_Fwd.Checked) { TB_Cipher.Text = result; }
                        if (Sel_Execute_Rev.Checked) { TB_Plain.Text = result;  }
                    }
                    else
                    {
                        MessageBox.Show("Пожалуйста, введите текст для обработки.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Пожалуйста, сгенерируйте ключ.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                string key = TB_Key.Text;
                if (key.Length > 0)
                {
                    string workstr = "";
                    if (Sel_Execute_Fwd.Checked) { workstr = TB_Plain.Text;  }
                    if (Sel_Execute_Rev.Checked) { workstr = TB_Cipher.Text; }
                    if (workstr.Length > 0)
                    {
                        string result = "";
                        while (workstr.Length % key.Length != 0)
                            { workstr += (char)(0); }

                        this.Cursor = Cursors.WaitCursor;
                        if (LB_CipherMethod.SelectedIndex == 0)
                        {   // Простой XOR-шифр:
                            Progress_Execute.Maximum = workstr.Length;
                            Progress_Execute.Value = 0;

                            for (int c = 0; c < workstr.Length; c++)
                            {
                                int res = key[c % key.Length];
                                    res = (workstr[c]) ^ res;
                                result += (char)(res);
                                Progress_Execute.Value = c + 1;
                            }
                        }
                        else
                        {   // Шифрование DES-ECB:
                            string[] keys = Cipher_DES.KeyMutMass(key);
                            Progress_Execute.Maximum = workstr.Length / 8;
                            Progress_Execute.Value = 0;

                            for (int m = 0; m < (workstr.Length / 8); m++)
                            {
                                string workbit = workstr.Substring(8 * m, 8);
                                if (Sel_Execute_Fwd.Checked)
                                    { result += Cipher_DES.Encrypt(workbit, keys); }
                                if (Sel_Execute_Rev.Checked)
                                    { result += Cipher_DES.Decrypt(workbit, keys); }

                                Progress_Execute.Value = m + 1;
                            }
                        }
                        this.Cursor = DefaultCursor;

                        if (Sel_Execute_Fwd.Checked) { TB_Cipher.Text = result; }
                        if (Sel_Execute_Rev.Checked) { TB_Plain.Text = result;  }
                    }
                    else
                    {
                        MessageBox.Show("Пожалуйста, введите текст для обработки.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Пожалуйста, сгенерируйте ключ.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        // Загрузить текст из файла:
        private void BnOpen_Click(object sender, EventArgs e)
        {
            DialogResult DR = DlgOpen.ShowDialog();
            if (DR == DialogResult.OK)
            {
                var FileContent = String.Empty;
                try
                {
                    using (FileStream fs = new FileStream(DlgOpen.FileName, FileMode.Open, FileAccess.Read))
                    {
                        byte[] Bytes = new byte[fs.Length];

                        int NumBytesToRead = (int)(fs.Length);
                        int NumBytesRead = 0;

                        while (NumBytesToRead > 0)
                        {
                            int n = fs.Read(Bytes, NumBytesRead, NumBytesToRead);

                            if (n == 0) { break; }

                            NumBytesRead += n;
                            NumBytesToRead -= n;
                        }

                        var wcencod = new UnicodeEncoding();
                        FileContent = wcencod.GetString(Bytes);
                    }
                }
                catch (FileNotFoundException ioEx)
                {
                    MessageBox.Show(ioEx.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (FileContent.Length > 0)
                {
                    // Вывод текста:
                    if (Sel_Execute_Fwd.Checked) { TB_Plain.Text = FileContent;  }
                    if (Sel_Execute_Rev.Checked) { TB_Cipher.Text = FileContent; }
                }
                else
                {
                    MessageBox.Show("Файл не содержит текстовой информации.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                if (DR != DialogResult.Cancel)
                {
                    MessageBox.Show("Произошла ошибка при открытии файла.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        // Сохранить текст в файл:
        private void BnSave_Click(object sender, EventArgs e)
        {
            var FileContent = String.Empty;
            if (Sel_Execute_Fwd.Checked) { FileContent = TB_Cipher.Text; }
            if (Sel_Execute_Rev.Checked) { FileContent = TB_Plain.Text; }

            if (FileContent.Length > 0)
            {
                DialogResult DR = DlgSave.ShowDialog();
                if (DR == DialogResult.OK)
                {
                    var FilePath = DlgSave.FileName;
                    if (FilePath.Length > 0)
                    {
                        var wcencod = new UnicodeEncoding();
                        byte[] Result = wcencod.GetBytes(FileContent);

                        using (FileStream fs = File.Open(FilePath, FileMode.OpenOrCreate))
                        {
                            fs.Write(Result, 0, Result.Length);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Не удалось создать пригодный для записи файл.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (DR != DialogResult.Cancel)
                    {
                        MessageBox.Show("Произошла ошибка при открытии файла.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Отсутствует пригодный для сохранения текст.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
