﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cipher_Sym
{
    class Cipher_DES
    {
        public static string KeyGen()
        {   // Генератор случайного ключа для шифрования DES.
            var key = new Char[8];
            var Rand = new Random();
            for (int i = 0; i < 8; i++)
            {
                // Создание случайного символа:
                key[i] = (Char)(Rand.Next(0, 128));
                key[i] = (Char)(key[i] << 1);

                // Определение конечного бита:
                bool finale = true;
                int temp = key[i];
                while (temp > 0)
                {
                    if (temp % 2 == 0)
                        { finale = !finale; }
                    temp = temp >> 1;
                }
                if (finale) { key[i] += (Char)(1); }
            }

            // Запись нового ключа в память:
            return new string(key);
        }
        
        public static string Encrypt(string Input, string[] Keys)
        {   // Шифрование блока алгоритмом DES-ECB:
            string Output = StringToByte(Input);
            Output = MassMut(Output, ip);

            for (int j = 0; j < Keys.Length; j++)
            {
                // Исполнение раундов прямого преобразования Фейстеля:
                string L = Output.Substring(0, Output.Length / 2);
                string R = Output.Substring(Output.Length / 2, Output.Length / 2);
                Output = R + StrXOR(FeiFunc(R, Keys[j]), L);
            }

            Output = MassMut(Output, final_permutation);
            return ByteToString(Output);
        }

        public static string Decrypt(string Input, string[] Keys)
        {   // Дешифрование блока алгоритмом DES-ECB:
            string Output = StringToByte(Input);
            Output = MassMut(Output, ip);

            for (int j = Keys.Length - 1; j >= 0; j--)
            {
                // Исполнение раундов обратного преобразования Фейстеля:
                string L = Output.Substring(0, Output.Length / 2);
                string R = Output.Substring(Output.Length / 2, Output.Length / 2);
                Output = StrXOR(FeiFunc(L, Keys[j]), R) + L;
            }

            Output = MassMut(Output, final_permutation);
            return ByteToString(Output);
        }

        private static string FeiFunc(string input, string key)
        {   // Рабочая функция преобразования Фейстеля:
            string output = "";

            string permut = MassMut(input, expansion);
            permut = StrXOR(permut, key);

            for (int k = 0; k < 8; k++)
            {
                string buf = "";
                for (int j = 0; j < 6; j++)
                {
                    buf += permut[k * 6 + j];
                }
                output += BlockMut(buf, k);
            }
            return MassMut(output, permutation_func);
        }

        private static string BlockMut(string input, int num)
        {
            string output = "";

            short a = 0;
            if (input[0] == '1') { a += 2; }
            if (input[5] == '1') { a += 1; }

            short b = 0;
            if (input[1] == '1') { b += 8; }
            if (input[2] == '1') { b += 4; }
            if (input[3] == '1') { b += 2; }
            if (input[4] == '1') { b += 1; }

            output = Convert.ToString(s_block[num, a, b], 2);
            while (output.Length < 4)
                { output = "0" + output; }
            return output;
        }

        public static string[] KeyMutMass(string key)
        {   // Получение массива преобразованных ключей:
            string[] Output = new string[key_shift.Length];

            string Result = MassMut(StringToByte(key), key_mut_0);

            string c = "", d = "";
            for (int i = 0; i < Result.Length / 2; i++)
            {
                c += Result[i];
                d += Result[i + Result.Length / 2];
            }

            int nom = 0;
            for (int i = 0; i < key_shift.Length; i++)
            {
                Output[i] = "";
                // Выполнение циклического сдвига:
                char buf = c[0], bufc = c[0], bufd = d[0];
                nom += key_shift[i];
                for (int q = 0; q < nom; q++)
                {
                    bufc = c[0]; c = c.Remove(0, 1); c += bufc;
                    bufd = d[0]; d = d.Remove(0, 1); d += bufd;
                }

                // Выборка бит ключа:
                Output[i] = c + d;
                Output[i] = MassMut(Output[i], key_chosen);
            }
            return Output;
        }

        private static string MassMut(string input, int[] permass)
        {
            string output = "";
            for (int k = 0; k < permass.Length; k++)
            {
                output += input[permass[k] - 1];
            }
            return output;
        }

        private static string StringToByte(string Inputs)
        {   // Перевод строки в двоичный формат:
            string Outputs = "";
            for (int i = 0; i < Inputs.Length; i++)
            {
                byte buf = (byte)(Inputs[i]);
                string char_binary = Convert.ToString(buf, 2);
                while (char_binary.Length < 8)
                    { char_binary = "0" + char_binary; }
                Outputs += char_binary;
            }
            return Outputs;
        }

        private static string ByteToString(string Inputs)
        {   // Перевод строки из двоичного формата:
            string Outputs = "";
            for (int i = 0; i < (Inputs.Length / 8); i++)
            {
                char TextCh = (char)(0);
                for (int l = 0; l < 8; l++)
                {
                    if (Inputs[8 * i + 7 - l] == '1')
                        { TextCh += (char)(1 << l); }
                }
                Outputs += TextCh.ToString();
            }
            return Outputs;
        }

        private static string StrXOR(string s1, string s2)
        {   // Побитовое XOR:
            string result = "";
            for (int i = 0; i < s1.Length; i++)
            {
                bool a = (s1[i] == '1');
                bool b = (s2[i] == '1');

                if (a ^ b) { result += "1"; }
                else { result += "0"; }
            }
            return result;
        }

        // ==================================================================================
        //                              ТАБЛИЦЫ ПРЕОБРАЗОВАНИЙ
        // Изначальная перестановка:
        private static readonly int[] ip = { 58, 50, 42, 34, 26, 18, 10, 2,
                                   60, 52, 44, 36, 28, 20, 12, 4,
                                   62, 54, 46, 38, 30, 22, 14, 6,
                                   64, 56, 48, 40, 32, 24, 16, 8,
                                   57, 49, 41, 33, 25, 17,  9, 1,
                                   59, 51, 43, 35, 27, 19, 11, 3,
                                   61, 53, 45, 37, 29, 21, 13, 5,
                                   63, 55, 47, 39, 31, 23, 15, 7 };
        // Расширение:
        private static readonly int[] expansion = {  32, 1,  2,  3,  4,  5,  4,  5,
                                                     6,  7,  8,  9,  8,  9,  10, 11,
                                                     12, 13, 12, 13, 14, 15, 16, 17,
                                                     16, 17, 18, 19, 20, 21, 20, 21,
                                                     22, 23, 24, 25, 24, 25, 26, 27,
                                                     28, 29, 28, 29, 30, 31, 32, 1   };
        // Перестановка ключевых бит:
        private static readonly int[] key_mut_0 = {  57, 49, 41, 33, 25, 17, 9,  1,
                                                      58, 50, 42, 34, 26, 18, 10, 2,
                                                      59, 51, 43, 35, 27, 19, 11, 3,
                                                      60, 52, 44, 36,
                                                      63, 55, 47, 39, 31, 23, 15, 7,
                                                      62, 54, 46, 38, 30, 22, 14, 6,
                                                      61, 53, 45, 37, 29, 21, 13, 5,
                                                                      28, 12, 20, 4  };
        // Выбор ключевых бит:
        private static readonly int[] key_chosen = {  14, 17, 11, 24, 1,  5,  3,  28,
                                                      15, 6,  21, 10, 23, 19, 12, 4,
                                                      26, 8,  16, 7,  27, 20, 13, 2,
                                                      41, 52, 31, 37, 47, 55, 30, 40,
                                                      51, 45, 33, 48, 44, 49, 39, 56,
                                                      34, 53, 46, 42, 50, 36, 29, 32 };
        // S-преобразование:
        private static readonly byte[, ,] s_block = {
                               {{0x0e, 0x04, 0x0d, 0x01, 0x02, 0x0f, 0x0b, 0x08, 0x03, 0x0a, 0x06, 0x0c, 0x05, 0x09, 0x00, 0x07},
                                {0x00, 0x0f, 0x07, 0x04, 0x0e, 0x02, 0x0d, 0x01, 0x0a, 0x06, 0x0c, 0x0b, 0x09, 0x05, 0x03, 0x08},
                                {0x04, 0x01, 0x04, 0x08, 0x0d, 0x06, 0x02, 0x0b, 0x0f, 0x0c, 0x09, 0x07, 0x03, 0x0a, 0x05, 0x00},
                                {0x0f, 0x0c, 0x08, 0x02, 0x04, 0x09, 0x01, 0x07, 0x05, 0x0b, 0x03, 0x0e, 0x0a, 0x00, 0x06, 0x0d}},
                               {{0x0f, 0x01, 0x08, 0x0e, 0x06, 0x0b, 0x03, 0x04, 0x09, 0x07, 0x02, 0x0d, 0x0c, 0x00, 0x05, 0x0a},
                                {0x03, 0x0d, 0x04, 0x07, 0x0f, 0x02, 0x08, 0x0e, 0x0c, 0x00, 0x01, 0x0a, 0x06, 0x09, 0x0b, 0x05},
                                {0x00, 0x0e, 0x07, 0x0b, 0x0a, 0x04, 0x0d, 0x01, 0x05, 0x08, 0x0c, 0x06, 0x09, 0x03, 0x02, 0x0f},
                                {0x0d, 0x08, 0x0a, 0x01, 0x03, 0x0f, 0x04, 0x02, 0x0b, 0x06, 0x07, 0x0c, 0x00, 0x05, 0x0e, 0x09}},
                               {{0x0a, 0x00, 0x09, 0x0e, 0x06, 0x03, 0x0f, 0x05, 0x01, 0x0d, 0x0c, 0x07, 0x0b, 0x04, 0x02, 0x08},
                                {0x0d, 0x07, 0x00, 0x09, 0x03, 0x04, 0x06, 0x0a, 0x02, 0x08, 0x05, 0x0e, 0x0c, 0x0b, 0x0f, 0x01},
                                {0x0d, 0x06, 0x04, 0x09, 0x08, 0x0f, 0x03, 0x00, 0x0b, 0x01, 0x02, 0x0c, 0x05, 0x0a, 0x0e, 0x07},
                                {0x01, 0x0a, 0x0d, 0x00, 0x06, 0x09, 0x08, 0x07, 0x04, 0x0f, 0x0e, 0x03, 0x0b, 0x05, 0x02, 0x0c}},
                               {{0x07, 0x0d, 0x0e, 0x03, 0x00, 0x06, 0x09, 0x0a, 0x01, 0x02, 0x08, 0x05, 0x0b, 0x0c, 0x04, 0x0f},
                                {0x0d, 0x08, 0x0b, 0x05, 0x06, 0x0f, 0x00, 0x03, 0x04, 0x07, 0x02, 0x0c, 0x01, 0x0a, 0x0e, 0x09},
                                {0x0a, 0x06, 0x09, 0x00, 0x0c, 0x0b, 0x07, 0x0d, 0x0f, 0x01, 0x03, 0x0e, 0x05, 0x02, 0x08, 0x04},
                                {0x03, 0x0f, 0x00, 0x06, 0x0a, 0x01, 0x0d, 0x08, 0x09, 0x04, 0x05, 0x0b, 0x0c, 0x07, 0x02, 0x0e}},
                               {{0x02, 0x0c, 0x04, 0x01, 0x07, 0x0a, 0x0b, 0x06, 0x08, 0x05, 0x03, 0x0f, 0x0d, 0x00, 0x0e, 0x09},
                                {0x0e, 0x0b, 0x02, 0x0c, 0x04, 0x07, 0x0d, 0x01, 0x05, 0x00, 0x0f, 0x0a, 0x03, 0x09, 0x08, 0x06},
                                {0x04, 0x02, 0x01, 0x0b, 0x0a, 0x0d, 0x07, 0x08, 0x0f, 0x09, 0x0c, 0x05, 0x06, 0x03, 0x00, 0x0e},
                                {0x0b, 0x08, 0x0c, 0x07, 0x01, 0x0e, 0x02, 0x0d, 0x06, 0x0f, 0x00, 0x09, 0x0a, 0x04, 0x05, 0x03}},
                               {{0x0c, 0x01, 0x0a, 0x0f, 0x09, 0x02, 0x06, 0x08, 0x00, 0x0d, 0x03, 0x04, 0x0e, 0x07, 0x05, 0x0b},
                                {0x0a, 0x0f, 0x04, 0x02, 0x07, 0x0c, 0x09, 0x05, 0x06, 0x01, 0x0d, 0x0e, 0x00, 0x0b, 0x03, 0x08},
                                {0x09, 0x0e, 0x0f, 0x05, 0x02, 0x08, 0x0c, 0x03, 0x07, 0x00, 0x04, 0x0a, 0x01, 0x0d, 0x01, 0x06},
                                {0x04, 0x03, 0x02, 0x0c, 0x09, 0x05, 0x0f, 0x0a, 0x0b, 0x0e, 0x01, 0x07, 0x06, 0x00, 0x08, 0x0d}},
                               {{0x04, 0x0b, 0x02, 0x0e, 0x0f, 0x00, 0x08, 0x0d, 0x03, 0x0c, 0x09, 0x07, 0x05, 0x0a, 0x06, 0x01},
                                {0x0d, 0x00, 0x0b, 0x07, 0x04, 0x09, 0x01, 0x0a, 0x0e, 0x03, 0x05, 0x0c, 0x02, 0x0f, 0x08, 0x06},
                                {0x01, 0x04, 0x0b, 0x0d, 0x0c, 0x03, 0x07, 0x0e, 0x0a, 0x0f, 0x06, 0x08, 0x00, 0x05, 0x09, 0x02},
                                {0x06, 0x0b, 0x0d, 0x08, 0x01, 0x04, 0x0a, 0x07, 0x09, 0x05, 0x00, 0x0f, 0x0e, 0x02, 0x03, 0x0c}},
                               {{0x0d, 0x02, 0x08, 0x04, 0x06, 0x0f, 0x0b, 0x01, 0x0a, 0x09, 0x03, 0x0e, 0x05, 0x00, 0x0c, 0x07},
                                {0x01, 0x0f, 0x0d, 0x08, 0x0a, 0x03, 0x07, 0x04, 0x0c, 0x05, 0x06, 0x0b, 0x00, 0x0e, 0x09, 0x02},
                                {0x07, 0x0b, 0x04, 0x01, 0x09, 0x0c, 0x0e, 0x02, 0x00, 0x06, 0x0a, 0x0d, 0x0f, 0x03, 0x05, 0x08},
                                {0x02, 0x01, 0x0e, 0x07, 0x04, 0x0a, 0x08, 0x0d, 0x0f, 0x0c, 0x09, 0x00, 0x03, 0x05, 0x06, 0x0b}}};
        // Функция преобразования:
        private static readonly int[] permutation_func = {   16, 7,  20, 21, 29, 12, 28, 17,
                                                             1,  15, 23, 26, 5,  18, 31, 10,
                                                             2,  8,  24, 14, 32, 27, 3,  9,
                                                             19, 13, 30, 6,  22, 11, 4,  25 };
        // Завершающая перестановка:
        private static readonly int[] final_permutation = {  40, 8,  48, 16, 56, 24, 64, 32,
                                                             39, 7,  47, 15, 55, 23, 63, 31,
                                                             38, 6,  46, 14, 54, 22, 62, 30,
                                                             37, 5,  45, 13, 53, 21, 61, 29,
                                                             36, 4,  44, 12, 52, 20, 60, 28,
                                                             35, 3,  43, 11, 51, 19, 59, 27,
                                                             34, 2,  42, 10, 50, 18, 58, 26,
                                                             33, 1,  41, 9,  49, 17, 57, 25 };
        // Массив сдвигов циклических перестановок:
        private static readonly byte[] key_shift = { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };
    }
}
