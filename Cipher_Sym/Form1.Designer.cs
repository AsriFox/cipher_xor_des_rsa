﻿namespace Cipher_Sym
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_Plain = new System.Windows.Forms.TextBox();
            this.TB_Cipher = new System.Windows.Forms.TextBox();
            this.Panel_Execute = new System.Windows.Forms.Panel();
            this.Bn_Execute = new System.Windows.Forms.Button();
            this.Sel_Execute_Rev = new System.Windows.Forms.RadioButton();
            this.Sel_Execute_Fwd = new System.Windows.Forms.RadioButton();
            this.Panel_Key = new System.Windows.Forms.Panel();
            this.TB_RSA_1 = new System.Windows.Forms.TextBox();
            this.TB_RSA_2 = new System.Windows.Forms.TextBox();
            this.TB_RSA_0 = new System.Windows.Forms.TextBox();
            this.Num_KeyLength = new System.Windows.Forms.NumericUpDown();
            this.Bn_Keygen = new System.Windows.Forms.Button();
            this.TB_Key = new System.Windows.Forms.TextBox();
            this.Progress_Execute = new System.Windows.Forms.ProgressBar();
            this.Bn_Exit = new System.Windows.Forms.Button();
            this.DlgOpen = new System.Windows.Forms.OpenFileDialog();
            this.DlgSave = new System.Windows.Forms.SaveFileDialog();
            this.BnOpen = new System.Windows.Forms.Button();
            this.BnSave = new System.Windows.Forms.Button();
            this.LB_CipherMethod = new System.Windows.Forms.ComboBox();
            this.Label_SK = new System.Windows.Forms.Label();
            this.Label_NK = new System.Windows.Forms.Label();
            this.Check_OK = new System.Windows.Forms.CheckBox();
            this.Panel_Execute.SuspendLayout();
            this.Panel_Key.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Num_KeyLength)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_Plain
            // 
            this.TB_Plain.Location = new System.Drawing.Point(9, 9);
            this.TB_Plain.Margin = new System.Windows.Forms.Padding(0);
            this.TB_Plain.Multiline = true;
            this.TB_Plain.Name = "TB_Plain";
            this.TB_Plain.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TB_Plain.Size = new System.Drawing.Size(360, 400);
            this.TB_Plain.TabIndex = 0;
            // 
            // TB_Cipher
            // 
            this.TB_Cipher.Location = new System.Drawing.Point(383, 9);
            this.TB_Cipher.Margin = new System.Windows.Forms.Padding(0);
            this.TB_Cipher.Multiline = true;
            this.TB_Cipher.Name = "TB_Cipher";
            this.TB_Cipher.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TB_Cipher.Size = new System.Drawing.Size(360, 400);
            this.TB_Cipher.TabIndex = 0;
            // 
            // Panel_Execute
            // 
            this.Panel_Execute.Controls.Add(this.Bn_Execute);
            this.Panel_Execute.Controls.Add(this.Sel_Execute_Rev);
            this.Panel_Execute.Controls.Add(this.Sel_Execute_Fwd);
            this.Panel_Execute.Location = new System.Drawing.Point(383, 409);
            this.Panel_Execute.Margin = new System.Windows.Forms.Padding(0);
            this.Panel_Execute.Name = "Panel_Execute";
            this.Panel_Execute.Size = new System.Drawing.Size(360, 28);
            this.Panel_Execute.TabIndex = 1;
            // 
            // Bn_Execute
            // 
            this.Bn_Execute.Location = new System.Drawing.Point(260, 4);
            this.Bn_Execute.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Execute.Name = "Bn_Execute";
            this.Bn_Execute.Size = new System.Drawing.Size(96, 20);
            this.Bn_Execute.TabIndex = 2;
            this.Bn_Execute.Text = "Выполнить";
            this.Bn_Execute.UseVisualStyleBackColor = true;
            this.Bn_Execute.Click += new System.EventHandler(this.Bn_Execute_Click);
            // 
            // Sel_Execute_Rev
            // 
            this.Sel_Execute_Rev.AutoSize = true;
            this.Sel_Execute_Rev.Location = new System.Drawing.Point(124, 4);
            this.Sel_Execute_Rev.Margin = new System.Windows.Forms.Padding(4);
            this.Sel_Execute_Rev.Name = "Sel_Execute_Rev";
            this.Sel_Execute_Rev.Size = new System.Drawing.Size(104, 17);
            this.Sel_Execute_Rev.TabIndex = 1;
            this.Sel_Execute_Rev.Text = "Дешифрование";
            this.Sel_Execute_Rev.UseVisualStyleBackColor = true;
            this.Sel_Execute_Rev.CheckedChanged += new System.EventHandler(this.Sel_Execute_Rev_CheckedChanged);
            // 
            // Sel_Execute_Fwd
            // 
            this.Sel_Execute_Fwd.AutoSize = true;
            this.Sel_Execute_Fwd.Location = new System.Drawing.Point(4, 4);
            this.Sel_Execute_Fwd.Margin = new System.Windows.Forms.Padding(4);
            this.Sel_Execute_Fwd.Name = "Sel_Execute_Fwd";
            this.Sel_Execute_Fwd.Size = new System.Drawing.Size(90, 17);
            this.Sel_Execute_Fwd.TabIndex = 0;
            this.Sel_Execute_Fwd.Text = "Шифрование";
            this.Sel_Execute_Fwd.UseVisualStyleBackColor = true;
            this.Sel_Execute_Fwd.CheckedChanged += new System.EventHandler(this.Sel_Execute_Fwd_CheckedChanged);
            // 
            // Panel_Key
            // 
            this.Panel_Key.Controls.Add(this.Num_KeyLength);
            this.Panel_Key.Controls.Add(this.Label_SK);
            this.Panel_Key.Controls.Add(this.Bn_Keygen);
            this.Panel_Key.Controls.Add(this.TB_RSA_2);
            this.Panel_Key.Controls.Add(this.TB_Key);
            this.Panel_Key.Location = new System.Drawing.Point(9, 409);
            this.Panel_Key.Margin = new System.Windows.Forms.Padding(0);
            this.Panel_Key.Name = "Panel_Key";
            this.Panel_Key.Size = new System.Drawing.Size(360, 28);
            this.Panel_Key.TabIndex = 1;
            // 
            // TB_RSA_1
            // 
            this.TB_RSA_1.Location = new System.Drawing.Point(293, 385);
            this.TB_RSA_1.Margin = new System.Windows.Forms.Padding(4);
            this.TB_RSA_1.MaxLength = 255;
            this.TB_RSA_1.Name = "TB_RSA_1";
            this.TB_RSA_1.Size = new System.Drawing.Size(72, 20);
            this.TB_RSA_1.TabIndex = 6;
            // 
            // TB_RSA_2
            // 
            this.TB_RSA_2.Location = new System.Drawing.Point(284, 4);
            this.TB_RSA_2.Margin = new System.Windows.Forms.Padding(4);
            this.TB_RSA_2.MaxLength = 255;
            this.TB_RSA_2.Name = "TB_RSA_2";
            this.TB_RSA_2.Size = new System.Drawing.Size(72, 20);
            this.TB_RSA_2.TabIndex = 6;
            // 
            // TB_RSA_0
            // 
            this.TB_RSA_0.Location = new System.Drawing.Point(82, 385);
            this.TB_RSA_0.Margin = new System.Windows.Forms.Padding(4);
            this.TB_RSA_0.MaxLength = 255;
            this.TB_RSA_0.Name = "TB_RSA_0";
            this.TB_RSA_0.Size = new System.Drawing.Size(72, 20);
            this.TB_RSA_0.TabIndex = 6;
            // 
            // Num_KeyLength
            // 
            this.Num_KeyLength.Location = new System.Drawing.Point(140, 4);
            this.Num_KeyLength.Margin = new System.Windows.Forms.Padding(4);
            this.Num_KeyLength.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.Num_KeyLength.Name = "Num_KeyLength";
            this.Num_KeyLength.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Num_KeyLength.Size = new System.Drawing.Size(48, 20);
            this.Num_KeyLength.TabIndex = 5;
            this.Num_KeyLength.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // Bn_Keygen
            // 
            this.Bn_Keygen.Location = new System.Drawing.Point(4, 4);
            this.Bn_Keygen.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Keygen.Name = "Bn_Keygen";
            this.Bn_Keygen.Size = new System.Drawing.Size(128, 20);
            this.Bn_Keygen.TabIndex = 1;
            this.Bn_Keygen.Text = "Генерировать ключ";
            this.Bn_Keygen.UseVisualStyleBackColor = true;
            this.Bn_Keygen.Click += new System.EventHandler(this.Bn_Keygen_Click);
            // 
            // TB_Key
            // 
            this.TB_Key.Location = new System.Drawing.Point(196, 4);
            this.TB_Key.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Key.MaxLength = 255;
            this.TB_Key.Name = "TB_Key";
            this.TB_Key.Size = new System.Drawing.Size(160, 20);
            this.TB_Key.TabIndex = 0;
            // 
            // Progress_Execute
            // 
            this.Progress_Execute.Location = new System.Drawing.Point(9, 440);
            this.Progress_Execute.Name = "Progress_Execute";
            this.Progress_Execute.Size = new System.Drawing.Size(734, 20);
            this.Progress_Execute.Step = 1;
            this.Progress_Execute.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Progress_Execute.TabIndex = 3;
            // 
            // Bn_Exit
            // 
            this.Bn_Exit.Location = new System.Drawing.Point(647, 467);
            this.Bn_Exit.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Exit.Name = "Bn_Exit";
            this.Bn_Exit.Size = new System.Drawing.Size(96, 24);
            this.Bn_Exit.TabIndex = 4;
            this.Bn_Exit.Text = "Выход";
            this.Bn_Exit.UseVisualStyleBackColor = true;
            this.Bn_Exit.Click += new System.EventHandler(this.Bn_Exit_Click);
            // 
            // DlgOpen
            // 
            this.DlgOpen.DefaultExt = "txt";
            // 
            // DlgSave
            // 
            this.DlgSave.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            this.DlgSave.RestoreDirectory = true;
            // 
            // BnOpen
            // 
            this.BnOpen.Location = new System.Drawing.Point(145, 467);
            this.BnOpen.Margin = new System.Windows.Forms.Padding(4);
            this.BnOpen.Name = "BnOpen";
            this.BnOpen.Size = new System.Drawing.Size(128, 24);
            this.BnOpen.TabIndex = 5;
            this.BnOpen.Text = "Загрузить из файла";
            this.BnOpen.UseVisualStyleBackColor = true;
            this.BnOpen.Click += new System.EventHandler(this.BnOpen_Click);
            // 
            // BnSave
            // 
            this.BnSave.Location = new System.Drawing.Point(281, 467);
            this.BnSave.Margin = new System.Windows.Forms.Padding(4);
            this.BnSave.Name = "BnSave";
            this.BnSave.Size = new System.Drawing.Size(128, 24);
            this.BnSave.TabIndex = 5;
            this.BnSave.Text = "Сохранить в файл";
            this.BnSave.UseVisualStyleBackColor = true;
            this.BnSave.Click += new System.EventHandler(this.BnSave_Click);
            // 
            // LB_CipherMethod
            // 
            this.LB_CipherMethod.DropDownHeight = 50;
            this.LB_CipherMethod.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(204)));
            this.LB_CipherMethod.FormattingEnabled = true;
            this.LB_CipherMethod.IntegralHeight = false;
            this.LB_CipherMethod.Items.AddRange(new object[] {
            "Простой XOR",
            "DES-ECB",
            "RSA"});
            this.LB_CipherMethod.Location = new System.Drawing.Point(9, 467);
            this.LB_CipherMethod.Margin = new System.Windows.Forms.Padding(4);
            this.LB_CipherMethod.MaxDropDownItems = 3;
            this.LB_CipherMethod.Name = "LB_CipherMethod";
            this.LB_CipherMethod.Size = new System.Drawing.Size(128, 24);
            this.LB_CipherMethod.TabIndex = 6;
            this.LB_CipherMethod.SelectedIndexChanged += new System.EventHandler(this.LB_CipherMethod_SelectedIndexChanged);
            // 
            // Label_SK
            // 
            this.Label_SK.Location = new System.Drawing.Point(188, 4);
            this.Label_SK.Margin = new System.Windows.Forms.Padding(4);
            this.Label_SK.Name = "Label_SK";
            this.Label_SK.Size = new System.Drawing.Size(88, 20);
            this.Label_SK.TabIndex = 7;
            this.Label_SK.Text = "Закрытый ключ";
            this.Label_SK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label_NK
            // 
            this.Label_NK.Location = new System.Drawing.Point(13, 385);
            this.Label_NK.Margin = new System.Windows.Forms.Padding(4);
            this.Label_NK.Name = "Label_NK";
            this.Label_NK.Size = new System.Drawing.Size(64, 20);
            this.Label_NK.TabIndex = 7;
            this.Label_NK.Text = "Основание";
            this.Label_NK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Check_OK
            // 
            this.Check_OK.Location = new System.Drawing.Point(179, 385);
            this.Check_OK.Margin = new System.Windows.Forms.Padding(4);
            this.Check_OK.Name = "Check_OK";
            this.Check_OK.Size = new System.Drawing.Size(106, 20);
            this.Check_OK.TabIndex = 8;
            this.Check_OK.Text = "Открытый ключ";
            this.Check_OK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Check_OK.UseVisualStyleBackColor = true;
            this.Check_OK.CheckedChanged += new System.EventHandler(this.Check_OK_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 502);
            this.Controls.Add(this.Check_OK);
            this.Controls.Add(this.Label_NK);
            this.Controls.Add(this.TB_RSA_1);
            this.Controls.Add(this.LB_CipherMethod);
            this.Controls.Add(this.BnSave);
            this.Controls.Add(this.TB_RSA_0);
            this.Controls.Add(this.BnOpen);
            this.Controls.Add(this.Bn_Exit);
            this.Controls.Add(this.Progress_Execute);
            this.Controls.Add(this.Panel_Key);
            this.Controls.Add(this.Panel_Execute);
            this.Controls.Add(this.TB_Cipher);
            this.Controls.Add(this.TB_Plain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Шифрообменник";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Panel_Execute.ResumeLayout(false);
            this.Panel_Execute.PerformLayout();
            this.Panel_Key.ResumeLayout(false);
            this.Panel_Key.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Num_KeyLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_Plain;
        private System.Windows.Forms.TextBox TB_Cipher;
        private System.Windows.Forms.Panel Panel_Execute;
        private System.Windows.Forms.Button Bn_Execute;
        private System.Windows.Forms.RadioButton Sel_Execute_Rev;
        private System.Windows.Forms.RadioButton Sel_Execute_Fwd;
        private System.Windows.Forms.Panel Panel_Key;
        private System.Windows.Forms.Button Bn_Keygen;
        private System.Windows.Forms.TextBox TB_Key;
        private System.Windows.Forms.ProgressBar Progress_Execute;
        private System.Windows.Forms.Button Bn_Exit;
        private System.Windows.Forms.NumericUpDown Num_KeyLength;
        private System.Windows.Forms.OpenFileDialog DlgOpen;
        private System.Windows.Forms.SaveFileDialog DlgSave;
        private System.Windows.Forms.Button BnOpen;
        private System.Windows.Forms.Button BnSave;
        private System.Windows.Forms.ComboBox LB_CipherMethod;
        private System.Windows.Forms.TextBox TB_RSA_2;
        private System.Windows.Forms.TextBox TB_RSA_0;
        private System.Windows.Forms.TextBox TB_RSA_1;
        private System.Windows.Forms.Label Label_SK;
        private System.Windows.Forms.Label Label_NK;
        private System.Windows.Forms.CheckBox Check_OK;
    }
}

